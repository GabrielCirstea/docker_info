# docker_info

A simple program to get some docker information, like number of images, containers,
volumes and so on

## Required info

TODO: add a nice table with the required information

* number of images
* number of containers
* number of healthy containers
* number of unhealthy containers
* number of volumes
* number of networks
* number of unused images
* number of unused volumes

## Testing

There is a "docker-compose.yml" file, and some docker files in "dockerfiles" directory.
Use `docker compose up` or `docker-compose up` to launch the containers, with some volumes
a network and healtcheck to have a "healthy" container.

Stop them with `docker compose down`
