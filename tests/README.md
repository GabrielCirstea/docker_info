# Testing

For the testing we'll use a set of json files for different scenarios.

## Getting the json content

Make a simple `curl` request to the docker API to get some info and save that
output to a file

```
curl --unix-socket /var/run/docker.sock http://localhost/networks
```
