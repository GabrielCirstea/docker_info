#!/bin/sh


# simple "script" to get all the data from the current docker env

curl --unix-socket /var/run/docker.sock http://localhost/containers/json?all=true > ./containers.json
curl --unix-socket /var/run/docker.sock http://localhost/images/json > ./images.json
curl --unix-socket /var/run/docker.sock http://localhost/networks > ./networks.json
curl --unix-socket /var/run/docker.sock http://localhost/volumes > ./volumes.json
