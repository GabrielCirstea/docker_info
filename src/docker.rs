use json;
use curl::easy::Easy;
pub struct DockerInfo {
    containers_all: json::JsonValue,
    images: json::JsonValue,
    networks: json::JsonValue,
    volumes: json::JsonValue,
}

pub enum DockerTypes {
    Containers,
    Images,
    Networks,
    Volumes,
    All
}

impl DockerInfo {
    pub fn new() -> Self {
        DockerInfo {
            containers_all: json::JsonValue::new_object(),
            images: json::JsonValue::new_object(),
            networks: json::JsonValue::new_object(),
            volumes: json::JsonValue::new_object(),
        }
    }

    /* initialize the structure, perform all the docker calls here */
    pub fn init(&mut self) {
        let content_all = self.perform_curl("containers/json?all=true").unwrap_or("".to_string());
        self.containers_all = json::parse(&content_all).unwrap_or(json::array!());
        let images = self.perform_curl("images/json").unwrap_or("".to_string());
        self.images = json::parse(&images).unwrap_or(json::array!());
        let content = self.perform_curl("volumes").unwrap_or("".to_string());
        self.volumes = json::parse(&content).unwrap();
        let content = self.perform_curl("networks").unwrap_or("".to_string());
        self.networks = json::parse(&content).unwrap();
    }

    /* refresh some or all data of the class */
    pub fn refresh(&mut self, value: DockerTypes) {
        match value {
            DockerTypes::Containers => {
                let content_all = self.perform_curl("containers/json?all=true").unwrap_or("".to_string());
                self.containers_all = json::parse(&content_all).unwrap_or(json::array!());
            },
            DockerTypes::Images => {
                let images = self.perform_curl("images/json").unwrap_or("".to_string());
                self.images = json::parse(&images).unwrap_or(json::array!());
            },
            DockerTypes::Volumes => {
                let content = self.perform_curl("volumes").unwrap_or("".to_string());
                self.volumes = json::parse(&content).unwrap();
            }
            DockerTypes::Networks => {
                let content = self.perform_curl("networks").unwrap_or("".to_string());
                self.networks = json::parse(&content).unwrap();
            }
            _ => self.init(),
        }
    }

    /* Performs a curl request to docker socket at "http://localhost"
     * @url - the rest of the url, e.g: "images/json"
     * @return - the JSON resons as a string */
    fn perform_curl(&self, url: &str) -> Option<String> {
        let mut content = Vec::new();
        let mut easy = Easy::new();
        let mut base_url = "http://localhost/".to_string();
        base_url.push_str(url);
        easy.url(&base_url).unwrap();
        easy.unix_socket("/var/run/docker.sock").unwrap();
        {
            let mut transfer = easy.transfer();
            transfer.write_function(|data| {
                content.extend_from_slice(data);
                Ok(data.len())
            }).unwrap();
            transfer.perform().unwrap();
        }
        return match String::from_utf8(content) {
            Ok(s) => Some(s),
            Err(why) => {
                println!("Error on curl: {}", why);
                None
            }
        }
    }

    /* Get all the ids for the images */
    pub fn images_ids(&mut self) -> Vec<String> {
        if self.images.is_empty() {
            let content_all = self.perform_curl("images/json").unwrap_or("".to_string());
            self.images = json::parse(&content_all).unwrap_or(json::array!());
        }
        let mut ids = Vec::new();
        for i in 0..self.images.len() {
            if !self.images[i]["Id"].is_null() {
                ids.push(self.images[i]["Id"].to_string())
            }
        }
        return ids;
    }

    /* Get the number of running containers and non-runing(stoped) containers
     * @return - (running, stoped) */
    pub fn no_containers(&mut self) -> (i32, i32) {
        if self.containers_all.is_empty() {
            let content_all = self.perform_curl("containers/json?all=true").unwrap_or("".to_string());
            self.containers_all = json::parse(&content_all).unwrap_or(json::array!());
        }
        let mut running = 0;
        for i in 0..self.containers_all.len() {
            if self.containers_all[i]["State"] == "running" {
                running += 1;
            }
        }
        return (running,(self.containers_all.len() as i32 - running));
    }

    /* Get the number of healthy and unhealthy containers
     * @return - (healthy, unhealthy) */
    pub fn containers_healthy(&self) -> (i32, i32) {
        // for each container
        let mut healthy = 0;
        let mut unhealthy = 0;
        for i in 0..self.containers_all.len() {
            let val = self.containers_all[i]["Status"].as_str().unwrap_or("");
            if val.contains("(healthy)") {
                healthy+=1;
            } else if val.contains("(unhealthy)") {
                unhealthy += 1;
            }
        }
        return (healthy, unhealthy);
    }

    /* @return - number of volumes*/
    pub fn no_volumes(&mut self) -> usize {
        if self.volumes.is_empty() {
            let content = self.perform_curl("volumes").unwrap_or("".to_string());
            self.volumes = json::parse(&content).unwrap();
        }
        if !self.volumes["Volumes"].is_null() {
            return self.volumes["Volumes"].len();
        } else {
            return 0;
        }
    }

    /* @return - number of networks*/
    pub fn no_networks(&mut self) -> usize {
        if self.networks.is_empty() {
            let content = self.perform_curl("networks").unwrap_or("".to_string());
            self.networks = json::parse(&content).unwrap();
        }
        self.networks.len()
    }
    /* Get the ID's of the used networks by the current created containers*/
    fn used_networks(&self) -> Vec<String> {
        let mut nets = Vec::new();
        for i in 0..self.containers_all.len() {
            // some of the bellow fields may be an array not an object in some cases
            nets.push(self.containers_all[i]["NetworkSettings"]["Networks"]
                      ["NetorkID"].to_string());
        }
        return nets;
    }
    pub fn unused_networks(&self) -> i32 {
        let nets = self.used_networks();
        let mut unused = 0;
        for i in 0..self.networks.len() {
            if !nets.contains(&self.networks[i]["Id"].to_string()) {
                unused += 1;
            }
        }
        return unused;
    }

    /* Search the images used in containers in the all the images on the docker
     * @return - number of unused images */
    pub fn unused_images(&mut self) -> i32 {
        let images_ids = self.images_ids();
        let mut conts_img_ids = Vec::new();
        for i in 0..self.containers_all.len() {
            let id = &self.containers_all[i]["ImageID"];
            if !id.is_null() {
                conts_img_ids.push(id.to_string());
            }
        }
        let mut number = 0;
        for id in images_ids {
            if !conts_img_ids.contains(&id) {
                number += 1;
            }
        }
        return number as i32;
    }

    /* Get the ids/names of the volumes created on docker
     * @return - vector of volume names */
    pub fn volume_ids(&self) -> Vec<String> {
        let mut ids = Vec::new();
        if self.volumes["Volumes"].is_null() {
            return ids;
        }
        for i in 0..self.volumes["Volumes"].len() {
            let name = &self.volumes["Volumes"][i]["Name"];
            if !name.is_null(){
                ids.push(name.to_string());
            }
        }
        return ids;
    }

    /* get all the volumes listed in the containers
     * @return - list of volume names */
    pub fn used_volumes(&self) -> Vec<String> {
        let mut vols = Vec::new();
        for i in 0..self.containers_all.len() {
            let mounts = &self.containers_all[i]["Mounts"];
            for j in 0..mounts.len() {
                if mounts[j]["Type"] == "volume" {
                    let name = &mounts[j]["Name"];
                    if !name.is_null() {
                        vols.push(name.to_string());
                    }
                }
            }
        }
        return vols;
    }

    /* Returns the number of unused volumes
     * @return unused volumes as i32 */
    pub fn ununsed_volumes(&self) -> i32 {
        let all_vols =  self.volume_ids();
        let used_vols = self.used_volumes();
        let mut unused = 0;
        for v in all_vols {
            if !used_vols.contains(&v) {
                unused+=1;
            }
        }
        return unused;
    }
}
