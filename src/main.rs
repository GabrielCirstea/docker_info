mod docker;
use docker::DockerTypes;

fn main() {
    println!("Hello, world!");
    let mut doc = docker::DockerInfo::new();
    doc.init();
    doc.refresh(DockerTypes::All);
    let (run, stop) = doc.no_containers();
    println!("Number of containers {}/{}", run, stop);
    let (healt, ill) = doc.containers_healthy();
    println!("Healthy and unhealty: {}/{}", healt, ill);
    println!("Number Images: {}", doc.images_ids().len());
    println!("Unused images: {}", doc.unused_images());
    println!("Number of volumes: {}", doc.no_volumes());
    println!("Unused Volumes: {}", doc.ununsed_volumes());
    println!("Number of networks: {}", doc.no_networks());
    println!("Unused networks: {}", doc.unused_networks());
}
